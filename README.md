# Proyecto Web Bootstrap 4

## Resumen
Este respositorio contiene las fuentes del proyecto del curso de boostrap4 de coursera en la Universidad Austral.

## Instrucciones
Descargue el repositorio y ejecute el comando npm install para descargar las dependencias utilizadas. (debe contar con nodejs + npm instalado en sus sistema)

# Licencia
Esta proyecto esta bajo la licencia MIT, la cual esta contenido en el archivo LICENSE en el directorio raíz del proyecto

# Información de contacto
Para ponerse en contacto conmigo puede escribirme al siguiente correo [manuelparra@live.com.ar](mailto:manuelparra@live.com.ar)
