
$(function() {
  // activamos los tolltips y los popover
  $("[data-toggle='tooltip']").tooltip();
  $("[data-toggle='popover']").popover();
  // modificamos el time de los slider a 2s
  $('.carousel').carousel({
    interval: 2000
  });
  // programamos un comportamiento en los botenes que activan el modal
  $('#contacto').on('show.bs.modal', function (e) {
    console.log('La ventana modal se esta cargando');

    id = e.relatedTarget.attributes[0].nodeValue;
    $('#' + id).removeClass('btn-primary');
    $('#' + id).addClass('btn-secondary');
    $('#' + id).prop('disabled', true);
  });
  $('#contacto').on('shown.bs.modal', function (e) {
    console.log('La ventana modal se cargo');
  })
  $('#contacto').on('hide.bs.modal', function (e) {
    console.log('La ventana modal se esta ocultando');

    $('#contacto-gran-melia').prop('disabled', false);
    $('#contacto-eurobuilding').prop('disabled', false);
    $('#contacto-renaissace').prop('disabled', false);
    $('#contacto-texaco').prop('disabled', false);
    $('#contacot-marriott').prop('disabled', false);

    $('#contacto-gran-melia').removeClass('btn-secondary');
    $('#contacto-gran-melia').addClass('btn-primary');

    $('#contacto-eurobuilding').removeClass('btn-secondary');
    $('#contacto-eurobuilding').addClass('btn-primary');

    $('#contacto-renaissace').removeClass('btn-secondary');
    $('#contacto-renaissace').addClass('btn-primary');

    $('#contacto-texaco').removeClass('btn-secondary');
    $('#contacto-texaco').addClass('btn-primary');

    $('#contacot-marriott').removeClass('btn-secondary');
    $('#contacot-marriott').addClass('btn-primary');
  })
  $('#contacto').on('hidden.bs.modal', function (e) {
    console.log('La ventana modal se oculto');
  })

  $('#contacot-marriott').click(function (e) {
    console.log('Clic sobre el boton');
  });
});
