'use strict';

// load plugins
const gulp = require('gulp');
const browsersync = require('browser-sync').create();
const sass = require('gulp-sass');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const del = require("del");
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const usemin = require('gulp-usemin');
const uglify = require('gulp-uglify');
const htmlmin = require('gulp-htmlmin');
const cleanCss = require('gulp-clean-css');
const rev = require('gulp-rev');
const flatmap = require('gulp-flatmap');

// browserSync
function browserSync(done) {
  browsersync.init({
    server: {
      baseDir: './'
    },
    port: 3000
  });
  done();
}

// browserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

// make css files from scss
function css() {
  return gulp
    .src('./assets/scss/*.scss')
    .pipe(plumber())
    .pipe(sass({ outputStyle: 'expanded' }))
    .pipe(gulp.dest('./assets/css'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(gulp.dest('./assets/css'))
    .pipe(browsersync.stream());
}

// clean public directory
function clean() {
  return del(['./public/']);
}

// optimize images
  function images() {
  return gulp
    .src('./assets/img/**/*')
    .pipe(newer('./public/assets/img'))
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.jpegtran({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: {
            removeViewBox: false,
            collapseGroups: true
          }
        })
      ])
    )
    .pipe(gulp.dest('./public/assets/img'));
}

// minificación de archivo htlm, css y js
function minify() {
  return gulp
    .src('./*.html')
    .pipe(flatmap(function(stream, file) {
      return stream
        .pipe(usemin({
          css: [rev()],
          html: [function() {return htmlmin({ collapseWhitespace: true })}],
          js: [uglify(), rev()],
          inlinejs: [uglify()],
          inlinecss: [cleanCss(), 'concat']
        }))
    }))
    .pipe(gulp.dest('public/'));
}

// copia las fuentes a la carpeta para deploy
function copyFonts() {
  return gulp
    .src('./node_modules/open-iconic/font/fonts/*.*')
    .pipe(gulp.dest('./public/assets/fonts'));
}

// watch files
function watchFiles() {
  gulp.watch('./assets/scss/*.scss', gulp.series(css, browserSyncReload));
  gulp.watch('./assets/js/*.js', browserSyncReload);
  gulp.watch("./assets/img/*.*", browserSyncReload);
  gulp.watch('./*.html', browserSyncReload);
}

// define complex tasks
const watch = gulp.parallel(watchFiles, browserSync);
const build = gulp.series(css, clean, gulp.parallel(images, copyFonts, minify));

exports.css = css;
exports.default = watch;
exports.browserSync = browserSync;
exports.images = images;
exports.clean =  clean;
exports.minify = minify;
exports.build = build;
exports.copyFonts = copyFonts;
