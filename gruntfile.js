module.exports = function(grunt) {
  require('time-grunt')(grunt);
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin'
  });

  // configuración de Grunt.js
  grunt.initConfig({
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'assets/scss',
          src: ['*.scss'],
          dest: 'assets/css',
          ext: '.css'
        }]
      }
    },
    watch: {
      files: ['assets/scss/*.scss'],
      tasks: ['css']
    },
    browserSync: {
      dev: {
        bsFiles: { // browser files
          src: [
            'assets/css/*.css',
            'assets/js/*.js',
            '*.html'
          ]
        },
        options: {
          watchTask: true,
          server: {
            baseDir: './' // directorio base para nuestro servidor
          }
        }
      }
    },
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'assets/img',
          src: '*.{png,gif,jpg,jpeg}',
          dest: 'public/assets/img'
        }]
      }
    },
    clean: {
      build: {
        src: ['public/'] // limpia la carpeta de deploy
      }
    },
    copy: {
      html: {
        files: [{
          expand: true,
          dot: true,
          cwd: './', // directorio de trabajo actual
          src: ['*.html'],
          dest: 'public'
        }]
      },
      fonts: {
        files: [{
          expand: true,
          dot: true,
          cwd: 'node_modules/open-iconic/font/fonts',
          src: '**/*',
          dest: 'public/assets/fonts'
        }]
      }
    },
    filerev: {
      options: {
        algorithm: 'md5',
        length: 15
      },
      files: {
        src: ['public/assets/css/*.css', 'public/assets/js/*.js']
      }
    },
    concat: {
      options: {
        separator: ';'
      },
      dist: {}
    },
    useminPrepare: {
      html: {
        dest: 'public',
        src: [
          'index.html',
          'about.html',
          'contact.html',
          'prices.html',
          'terms.html'
        ]
      },
      options: {
        dest: 'public',
        flow: {
          steps: {
            css: ['cssmin'],
            js: ['uglify']
          },
          post: {
            css: [{
              name: 'cssmin',
              createConfig: function(context, block) {
                var generated = context.options.generated;
                generated.options = {
                  keepSpecialComments: 0,
                  rebase: false
                }
              }
            }]
          }
        }
      }
    },
    usemin: {
      html: [
        'public/index.html',
        'public/about.html',
        'public/contact.html',
        'public/prices.html',
        'public/terms.html'
      ],
      options: {
        assetsDirs: ['public', 'public/assets/css', 'public/assets/js']
      }
    },
    htmlmin: {
      site: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        files: [{
          expand: true,
          cwd: 'public', // directorio de trabajo actual
          src: ['*.html'],
          dest: 'public'
        }]
      }
    }
  });

  // loadNpmTask
  // grunt.loadNpmTasks('grunt-contrib-sass');
  // grunt.loadNpmTasks('grunt-contrib-watch');
  // grunt.loadNpmTasks('grunt-browser-sync');
  // grunt.loadNpmTasks('grunt-contrib-imagemin');

  // Registro de tareas
  grunt.registerTask('css', ['sass']);
  grunt.registerTask('img:compress', ['imagemin'])
  grunt.registerTask('default', ['browserSync', 'watch']);
  grunt.registerTask('build', [
    'clean', // borramos el contenido de public/
    'copy', // copiamos las fonts a public/assets/fonts
    'imagemin', // optimizamos imagenes y las copiamos a public
    'useminPrepare', // preparamos la configuracion de usemin
    'concat',
    'cssmin',
    'uglify',
    'filerev', // agregamos cadena aleatoria
    'usemin',  // reemplazamos las referencias por los archivos generados por filerev
    'htmlmin' // minifica los html y en public/
  ]);
};
